$(document).ready(function() {
    var last_name = $('#last-name');
    var name = $('#name');
    var surname = $('#surname');
    var progress = parseInt($('#progress').attr('aria-valuemax')); // получаем максимально возможное значение
    var all = $('.validate').length; // получаем количество полей для валидации

    function progressBar(){
        var percent_one = progress / all; // поулчаем значение прогресс бара для одного поля
        var with_error = $('.validate.error').length; // получаем значение полей с ошибкой


        if (with_error == 0){ // если ошибок 0 то длина прогрессбара будет 100
            var current = 100;
            var opacity = 99;
        } else {
            var current = (all - with_error) * percent_one; // подсчет насколько заполнить прогрессбар
            var opacity = current.toFixed(0);
        }

        $('#progress').attr('aria-valuenow', current.toFixed(1));
        $('#progress').css('width', current + '%');
        $('#progress').find('.overlay').css('opacity', '0.' + opacity);
    }

    var checkForm = {
        'last_name_check' : function(){
            if (last_name.val().length <= 2) {
                $(last_name).addClass('error');
            }else{
                $(last_name).removeClass('error');
            }
            progressBar();
        },

        'name_check' : function(){
            if (name.val().length <= 2) {
                $(name).addClass('error');
            }else{
                $(name).removeClass('error');
            }
            progressBar();
        },

        'surname_check' : function(){
            if (surname.val().length <= 2) {
                $(surname).addClass('error');
            }else{
                $(surname).removeClass('error');
            }
            progressBar();
        }
    }

    $('#last-name').on('keypress keyup blur touchend', checkForm.last_name_check);
    $('#name').on('keypress keyup blur touchend', checkForm.name_check);
    $('#surname').on('keypress keyup blur touchend', checkForm.surname_check);

    $(document).on('keypress keyup blur touchend', '#just-number', function () {
        var res = $(this).val();
        $(this).val(res.replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }

        if (res > 100) {
            $(this).val(100);
        }
    });

    $(document).on('click', '#set-progress', function () {
        var number = parseInt($('#just-number').val());

        if (number >= 0) {
            $('#just-number').removeClass('error');

            var current_number = parseInt($('#number-progress').text());

            if (number >= current_number){ // тут мне просто скучно стало
                interval = setInterval(function () {
                    var prev_number = parseInt($('#number-progress').text());
                    var new_number = prev_number + 1;
                    $('#number-progress').text(new_number);
                    if (new_number >= number){
                        clearInterval(interval);
                        $('#number-progress').text(number);
                    }
                },10);
            } else {
                interval = setInterval(function () {
                    var prev_number = parseInt($('#number-progress').text());
                    var new_number = prev_number - 1;
                    $('#number-progress').text(new_number);
                    if (new_number == number || new_number < 1){
                        clearInterval(interval);
                        $('#number-progress').text(number);
                    }
                },10);
            }

            $('#number-progress').css('width', number + '%'); // а этот кусок нужен. =)
        }else{
            $('#just-number').addClass('error');
        }

    });

    $(document).on('click', '#show-resume', function () {
        $('.popup').fadeIn('normal');
        $('.overflow').fadeIn('normal');
    });

    $(document).on('click', '.popup .close, .overflow', function () {
        $('.popup').fadeOut('normal');
        $('.overflow').fadeOut('normal');
    });
});